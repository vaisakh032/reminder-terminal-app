from terminalColour import colors as cl
import json
import os
import datetime
import sys

reminder_file = str(os.path.expanduser('~'))+"/.remind.json"
id_top = 0
reminders = {}

def get_date_and_time():
    while 1:
        date_text = input("Enter date (DD/MM/YY): ")
        if len(date_text)==1 and date_text == '.':
            date_text = datetime.datetime.today().strftime('%d-%m-%Y')
        if len(date_text)==2 and date_text == '..':
            date_text = (datetime.date.today() + datetime.timedelta(days=1)).strftime('%d-%m-%Y')

        _strp_charac = '.'
        if '/' in date_text:
            _strp_charac = '/'
        elif ':' in date_text:
            _strp_charac = ':'
        elif '-' in date_text:
            _strp_charac = '-'

        if int(date_text.split(_strp_charac)[2])<1000:
            date_text = date_text.split(_strp_charac)[0]+_strp_charac+date_text.split(_strp_charac)[1]+_strp_charac+str(int(date_text.split(_strp_charac)[2])+2000)         
        try:
            remind_date = datetime.date(int(date_text.split(_strp_charac)[2]), int(date_text.split(_strp_charac)[1]), int(date_text.split(_strp_charac)[0]))
            print("\t\t\t"+cl.GREEN,remind_date.strftime("%A"),cl.GREY+" ",remind_date,cl.END)
            break
        except ValueError:
            print("Invalid date format!")
            continue

    while 1:
        time_text = input("Time (HH:MM)/(HH:MM Am/Pm): ").lower()
        if time_text == '' or time_text == ".":
            remind_time = -1
            print("\t\t\t"+cl.GREEN,'Whole day',cl.END)
            break
        _strp_charac = '.'
        if '/' in date_text:
            _strp_charac = '/'
        if ':' in date_text:
            _strp_charac = ':'
        if 'p' in time_text.lower():
            time_text = str(int(time_text.split(_strp_charac)[0])+12)+_strp_charac+time_text.split(_strp_charac)[1]
        time_text = time_text.strip("am")
        time_text = time_text.strip("pm")
        try:
            remind_time = datetime.time(int(time_text.split(_strp_charac)[0]), int(time_text.split(_strp_charac)[1]),00)
            print("\t\t\t"+cl.GREEN,remind_time,cl.END)
            break
        except ValueError:
            print("Invalid time format!")
            continue
    print(cl.GREY,"Remind Me on\n",cl.YELLOW,remind_date.strftime("%A")," ",remind_date, " at ", remind_time," to:", cl.END)        
    return(remind_date,remind_time)

def init_with_file():
    global id_top
    if os.path.exists(reminder_file):
        file = open(reminder_file,'r')
        reminder_data = json.load(file)
        id_top = int(sorted(reminder_data.keys())[-1])
        return reminder_data
    else:
        print("Start by addind reminders")
        id_top = 999;
        return {}

def addReminder(newdata):
    global id_top
    global reminders
    id_top = id_top + 1
    reminders[id_top] = newdata['payload']
    update_reminder()

def update_reminder():
    global reminders
    with open(reminder_file, 'w') as outfile:
        json.dump(reminders, outfile)

def get_new_reminder_data(id_update=-1):
    payload = {}
    dataDict = {}
    (date_text,time_text) = get_date_and_time()
    message = input("Enter message: ")
    priority = input("Enter Priority (^1,5_): ")
    payload['date'] = str(date_text)
    payload['priority'] = priority
    payload['time'] = str(time_text)
    payload['message'] = str(message)
    payload['complete'] = 'False'

    if id_update == -1:
        dataDict['payload'] = payload
        addReminder(dataDict)
    else:
        global reminders
        reminders[id_update] = payload
        update_reminder()

def display_reminders(id, printDate=True):
    global reminders
    payload = []
    if id in reminders.keys():
        payload = reminders[id]

        if printDate:
            print(cl.GREY+str(id)+" "+cl.OKBLUE+payload['date'],end=' ')
        else:
            print(" "*2+cl.GREEN+">>",end=' ')
        modifier = ''
        if int(payload['priority'])<=2:
            modifier = cl.FAIL
        elif int(payload['priority'])<=4:
            modifier = cl.YELLOW
        else:
            modifier = cl.HEADER

        if payload['complete'] == 'True':
            modifier = modifier + '\x1b[9m'

        print(modifier+payload['message'],cl.GREY+"  |"+str(id),cl.END)


def toggle_complete(id):
    global reminders
    if id in reminders.keys():
        payload = reminders[id]
        print(payload['complete'])
        payload['complete'] = str(not eval(payload['complete']))
        print(payload['complete'])
        reminders[id] = payload
        display_reminders(id)
        update_reminder()
        print(cl.GREY+"Reminder data updated",cl.END)
    else:
         print(cl.FAIL+"Reminder not found!",cl.END)


def check_fit_for_display(payload):
    if eval(payload['complete']) == True:
        return False
    todays_date = datetime.datetime.today().strftime('%Y-%m-%d')
    todays_date = datetime.datetime.strptime(todays_date, "%Y-%m-%d").date()
    remind_date = datetime.datetime.strptime(payload['date'], "%Y-%m-%d").date()

    if remind_date <= todays_date:
        return True
    else:
        return False

def sort_dates_with_ids(reminders):
    dates = []
    groupDates = {}
    for key , value in reminders.items():
        if value['date'] not in groupDates.keys():
            groupDates[value['date']] = [key]
        else:
            groupDates[value['date']].append(key)
        if value['date'] not in dates:
            dates.append(value['date'])
    sortedDates = sorted(dates, key=lambda x: datetime.datetime.strptime(x, "%Y-%m-%d").strftime("%Y-%m-%d"))
    returnId = []
    for values in sortedDates:
        for ids in groupDates[values]:
            returnId.append(ids)
    return(returnId)

def get_reminders_onebyone(show_all = False):
    global reminders
    totalNumberOfReminders = 0
    idArray = sort_dates_with_ids(reminders)
    lastDatePrinted = ''
    for ids in idArray:
        payload = reminders[ids]
        if check_fit_for_display(payload) or show_all:
            if lastDatePrinted != payload['date']:
                print(cl.OKBLUE+payload['date'],'-'*35+cl.END)
            display_reminders(ids,printDate=False)   
            lastDatePrinted = payload['date']
            totalNumberOfReminders = totalNumberOfReminders + 1
    
    if totalNumberOfReminders == 0:
        print(cl.GREEN+"Be happy, you have no Reminders!"+cl.END)
    print(cl.GREY+"-----------------------------------------------"+cl.END)    


if __name__ == "__main__":
    reminders = init_with_file()
    args = sys.argv
    print(cl.GREY+"-----------------Reminders--------------------"+cl.END) 

    if len(args) == 1:
        get_reminders_onebyone()
        exit()
    elif args[1] == '-a':
        get_reminders_onebyone(True)
        exit()    
    elif args[1] == '-t':
        id = input("Enter the reminder id: ")
        toggle_complete(id)
    elif args[1] == '-n':
        get_new_reminder_data()
    elif args[1] == '-u':
        id = input("Enter the reminder id: ")
        get_new_reminder_data(id)        
    
